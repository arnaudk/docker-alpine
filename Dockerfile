# Define Alpine Linux as the base image
FROM alpine:3.8

# Define the maintainer
LABEL maintainer="Arnaud Klipfel <arnaud@klipfel.net>"

# Update list of packages and upgrade outdated packages
RUN apk update && \
    apk upgrade

# Install useful packages
RUN apk add nano unzip bash curl

# Clean the directories
RUN rm -rf /var/cache/apk/*

# Set the timezone
RUN apk --no-cache add tzdata && \
    cp /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    echo "Europe/Paris" > /etc/timezone && \
    apk del tzdata
